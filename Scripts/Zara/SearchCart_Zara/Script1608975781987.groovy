import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://www.zara.com/id/')

WebUI.click(findTestObject('Object Repository/Zara/Cart_Homepage/btn_login'))

WebUI.setText(findTestObject('Object Repository/Zara/Cart_LoginPage/txt_username'), 'yeaayabc@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Zara/Cart_LoginPage/txt_password'), 'uei5vx79Jy+sRXVzf9JI4A==')

WebUI.click(findTestObject('Object Repository/Zara/Cart_LoginPage/btn_login'))

WebUI.click(findTestObject('Zara/Page_ZARA Indonesia  SALE/a_cari'))

WebUI.setText(findTestObject('Object Repository/Zara/Cart_Search_ZARA/input_Cari_searchTerm'), 'Tas')

WebUI.sendKeys(findTestObject('Object Repository/Zara/Cart_Search_ZARA/input_Cari_searchTerm'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Zara/Cart_Search_ZARA/img_tas'))

WebUI.click(findTestObject('Object Repository/Cart_Tas/span_M'))

WebUI.click(findTestObject('Object Repository/Cart_Tas/button_Tambah'))

WebUI.click(findTestObject('Object Repository/Cart_Tas/a_Lihat tas belanja'))

