<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_username</name>
   <tag></tag>
   <elementGuidId>349f0981-6e03-4fa5-9bc3-7c2ca4c27f86</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;email&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='email']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-input-label__input form-input-text__input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>email</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>form-input__label-email</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>form-input__desc-email</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main&quot;)/article[@class=&quot;layout__article&quot;]/div[@class=&quot;layout-user__content&quot;]/div[@class=&quot;logon-view__content&quot;]/section[@class=&quot;logon-view__section&quot;]/form[@class=&quot;form form--one-column logon-view__form&quot;]/div[@class=&quot;form__fields&quot;]/div[@class=&quot;form__column&quot;]/div[@class=&quot;form-input form-input--has-help form-input--has-focus&quot;]/div[@class=&quot;form-input__wrapper&quot;]/div[@class=&quot;form-input-label&quot;]/input[@class=&quot;form-input-label__input form-input-text__input&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='email']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='main']/article/div/div/section/form/div/div/div/div/div/input</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
   </webElementXpaths>
</WebElementEntity>
